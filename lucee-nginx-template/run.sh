#! /bin/sh
set -e

# Run setup scripts
run-parts --exit-on-error /opt/lucee/setup.d

# Launch services
exec supervisord -c /etc/supervisor/supervisord.conf