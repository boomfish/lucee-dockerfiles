# Lucee on Tomcat with nginx and configuration templating

[Lucee](http://www.lucee.org/) CFML application engine running on [Apache Tomcat](https://tomcat.apache.org/) J2EE application server and [nginx](http://nginx.org/) HTTP server with Perl Template Toolkit for generating configuration files.

## Features

### Java optisation tweaks

Lucee 4's [Java Agent](http://blog.getrailo.com/post.cfm/railo-4-1-smarter-template-compilation) is enabled for better memory management of compiled CFML code.

JVM is set to [use /dev/urandom as an entropy source for secure random numbers](http://support.run.pivotal.io/entries/59869725-Java-Web-Applications-Slow-Startup-or-Failing) to avoid blocking Tomcat on startup.

Tomcat is configured to [skip the default scanning of Jar files on startup](http://www.gpickin.com/index.cfm/blog/how-to-get-your-tomcat-to-pounce-on-startup-not-crawl), significantly improving startup time.

A compatible version of the [Apache Tomcat Native Library](http://tomcat.apache.org/native-doc/) is installed for better Tomcat performance in production environments.

### Optimised for single-site application

The default configuration serves a single application for any hostname on the listening port. Multiple applications can be supported by editing the server.xml in the Tomcat config.

### Template generator

This container contains a custom version of the [tpage](http://www.template-toolkit.org/docs/tools/tpage.html) command-line tool for the Perl [Template Toolkit](http://www.template-toolkit.org/).

This tool can read environment variables and feed them as variables into templates. This is useful for generating server configuration files from template files either at build time (in a Dockerfile) or run time (in a startup script).

### Session management

The default template for generating lucee-server.xml sets the session type to "j2ee". This setting avoids some issues for Lucee servers running in Docker containers.

## Using this image

### Accessing the service

The default configuration has nginx listening on port 80 and Tomcat listening on port 8080 in the container.

This image exposes ports 80, 443 (in case you enable SSL in nginx), and 8080 to linked containers, however you must publish the ports if you wish to access them from the Docker host.

### Accessing the Web admin

The Lucee admin URL is `/lucee/admin/` from the exposed port. No admin passwords are set by default, but see environment variables below.

**THIS IS NOT A SECURE CONFIGURATION FOR PRODUCTION ENVIRONMENTS!** It is **strongly** recommended that you secure the container by:

- Changing the server password
- Using IP or URL filtering to restrict access to the Lucee web admin
- Following recommended security practices such as the [Lucee Lockdown Guide](https://bitbucket.org/lucee/lucee/wiki/tips_and_tricks_Lockdown_Guide)

### Sample CFML page

The default webroot contains a simple "Hello world" index.cfm file which dumps the CFML server scope. This can be replaced with your own CFML code and assets in derived images.

### Locations of folders and configuration files

Web root for default site: /var/www

Configuration folders:

- Tomcat config: /usr/local/tomcat/conf
- Lucee config for default site: /opt/lucee/web
- Lucee server context: /opt/lucee/server/lucee-server/context
- nginx config: /etc/nginx
- supervisor config: /etc/supervisor
- Configuration setup scripts: /opt/lucee/setup.d

Templates:

- Perl template for nginx.conf: /etc/nginx/templates/nginx.conf
- Perl template for default.conf: /etc/nginx/templates/default.conf
- Perl template for lucee-server.xml: /opt/lucee/templates/lucee_server.xml
- Perl template for lucee-web.xml.cfm: /opt/lucee/templates/lucee_web.xml

Log folders:

- Tomcat logs: /var/log/tomcat (/usr/local/tomcat/logs is a symlink to /var/log/tomcat)
- Lucee logs for default site: /var/log/lucee (/opt/lucee/web/logs is a symlink to /var/log/lucee)
- nginx logs: /var/log/nginx
- supervisor logs: /var/log/supervisor

### Environment variables

The default image contains scripts that use the following environment variables if they are set in the container.

`LUCEE_JAVA_OPTS`: Additional JVM parameters for Tomcat. Used by /usr/local/tomcat/bin/setenv.sh. Default: "-Xms256m -Xmx512m -XX:MaxPermSize=128m".

`LUCEE_SERVER_ADMIN_PASSWORD_HSPW`: Password hash for /lucee/admin/server.cfm login. Used by /opt/lucee/templates/lucee_server.xml. Default: "".

`LUCEE_SERVER_ADMIN_PASSWORD_SALT`: Password salt for /lucee/admin/server.cfm login. Used by /opt/lucee/templates/lucee_server.xml. Default: "".

`LUCEE_WEB_ADMIN_PASSWORD_HSPW`: Password hash for /lucee/admin/web.cfm login. Used by /opt/lucee/templates/lucee_web.xml. Default: "".

`LUCEE_WEB_ADMIN_PASSWORD_SALT`: Password salt for /lucee/admin/web.cfm login. Used by /opt/lucee/templates/lucee_web.xml. Default: "".

`NGINX_WORKER_PROCESSES`: The number of nginx worker processes. Default: 4

`NGINX_WORKER_CONNECTIONS`: The maximum number of simultaneous connections that can be opened by an nginx worker process. Default: 768

`NGINX_SENDFILE`: Controls the use of sendfile() in nginx. Set to "off" when serving content from networked or VM-mapped folders. Default: on
