# Dockerfiles for Lucee application servers

This repository contains Dockerfiles to build Lucee application servers

## Features

### Java optisation tweaks

Lucee 4's [Java Agent](http://blog.getrailo.com/post.cfm/railo-4-1-smarter-template-compilation) is enabled for better memory management of compiled CFML code.

JVM is set to [use /dev/urandom as an entropy source for secure random numbers](http://support.run.pivotal.io/entries/59869725-Java-Web-Applications-Slow-Startup-or-Failing) to avoid blocking Tomcat on startup.

Tomcat is configured to [skip the default scanning of Jar files on startup](http://www.gpickin.com/index.cfm/blog/how-to-get-your-tomcat-to-pounce-on-startup-not-crawl), significantly improving startup time.

A compatible version of the [Apache Tomcat Native Library](http://tomcat.apache.org/native-doc/) is installed for better Tomcat performance in production environments.

### Optimised for single-site application

The default configuration serves a single application for any hostname on the listening port. Multiple applications can be supported by editing the server.xml in the Tomcat config.

### Template generator

The lucee-template image contains a custom version of the [tpage](http://www.template-toolkit.org/docs/tools/tpage.html) command-line tool for the Perl [Template Toolkit](http://www.template-toolkit.org/).

This tool can read environment variables and feed them as variables into templates. This is useful for generating server configuration files from template files either at build time (in a Dockerfile) or run time (in a startup script).

## Details on Docker images

- [Lucee on Tomcat](lucee-tomcat/README.md)
- [Lucee on Tomcat with nginx](lucee-nginx/README.md)
- [Lucee on Tomcat with nginx and configuration templating](lucee-nginx-template/README.md)

## Prebuilt images on Docker Hub registry

Prebuilt Docker images are available on [Docker Hub](https://hub.docker.com/). These images are are created via [automated builds](https://docs.docker.com/docker-hub/builds/).

These images are not 'trusted' and are provided with no warranty. You may find these prebuilt images useful for trying out the containers, but for any serious work I recommend you fork the GitHub repository and use your fork to build your own images. Everything needed to build 

You can find the prebuilt images in the following repositories:

- [Lucee on Tomcat](https://registry.hub.docker.com/u/boomfish/railo-tomcat/)
- [Lucee on Tomcat and nginx](https://registry.hub.docker.com/u/boomfish/railo-nginx/)
- [Lucee on Tomcat with nginx and configuration templating](https://registry.hub.docker.com/u/boomfish/railo-nginx-template/)

## License

The Docker files and config files are available under the [MIT License](LICENSE.txt). The CFML engines are available under their respective licenses.